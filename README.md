**Description**
This project revolves around the development of an engaging game that features a central agent navigating through a dynamic map while facing off against various enemy agents. The focus of the project includes implementing sophisticated AI for the main agent, progressively increasing in complexity to match the game's difficulty levels. Additionally, animations play a pivotal role in enhancing the gaming experience, with numerous animations incorporated to accommodate the agent's actions and movements.

![fight animation](latex/fight_animation.png)

**Command line instructions**
You can also upload existing files from your computer. Using the instructions below you play the game.

```
git clone https://gitlab.com/MRomano1/CallSP.git
cd CallSP
http-server -c
```

Navigateto 127:0:0:1:8081

Have fun ;)

**Main Components**

AI Development: The core of the project involves creating an intelligent AI for the central agent. This AI's sophistication is tailored to different difficulty levels, adding depth and challenge to the gameplay. The AI's decision-making abilities are fine-tuned to ensure a rewarding gaming experience.

Agent-Enemy Interaction: The game's dynamics hinge on the interactions between the central agent and the enemy agents. The central agent's strategies for fighting against multiple enemy agents form a significant aspect of the game's appeal.

Animation: Animations play a crucial role in immersing players in the game world. A variety of animations are meticulously designed and integrated, enhancing the visual appeal and realism of the game. These animations capture the agent's actions, interactions, and movements.

The grass moves with wind and can be enabled ( WARNING: i7 1.2ghz < 18fps)
![fight animation](latex/grass_move.png)

**Achievements**

AI Complexity: The project showcases the progression of AI complexity in correlation with the game's difficulty levels. This provides players with a challenging and engaging experience.
Dynamic Gameplay: The interaction between the central agent and enemy agents introduces dynamic and unpredictable elements into the gameplay.
Immersive Animations: meticulously crafted animations elevate the game's visual quality.
1. Tools and Environment:
The project relies on several tools and libraries to create a seamless gaming experience:

WebGL: Utilized to manually create assets, particularly for shading effects like bulbLight from ThreeJS. Fine-tuning of textures is performed to achieve the desired visual quality.
Blender: Used for intricate adjustments of armatures, character models, and textures. Models from WebGL are often modified and merged with Blender-created assets.
TweenJS: This library is employed for spline interpolation to create smooth animations. Functions for interpolation are designed for each class, and the Tween_group library assists in moving joints as specified in animations.
Future Enhancements

Incorporating adaptive AI that learns from player behavior.
Exploring procedural animation techniques to add more fluidity to character movements.
Integrating multiplayer features to enhance the agent-enemy interactions.


**Author:** Matteo Romano

